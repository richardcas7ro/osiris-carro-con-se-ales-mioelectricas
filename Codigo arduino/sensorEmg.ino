#include <Servo.h>  
Servo motorCar;     
int EMGsig;           // variable para almacenar el valor del sensor
int servoPosicion;    // Posicion para angulo del motor
int rango = 100;  //Establecemos rango para comparar los movimientos del motor
int ledBlue = 13;      //conectamos led a este pin para visualizar cambios
int Pin_Motor_Der_A = 8;
int Pin_Motor_Der_B = 9;
int Pin_Motor_Izq_A = 10;
int Pin_Motor_Izq_B = 11;

void setup() {
 Serial.begin(9600); // Starting the communication with the computer
 motorCar.attach(9); // Tell the servo it is plugged into pin 9
 pinMode(Pin_Motor_Der_A, OUTPUT);
  pinMode(Pin_Motor_Der_B, OUTPUT);
  pinMode(Pin_Motor_Izq_A, OUTPUT);
  pinMode(Pin_Motor_Izq_B, OUTPUT);
}
void loop() {
 EMGsig = analogRead(A0); // Leemos valores del sensor EMG
if (EMGsig < rango){   
    servoPosicion = 20; 
    digitalWrite(ledBlue, LOW);
  } else{            
   servoPosicion=map(EMGsig,rango,1023,20,160);
   digitalWrite(ledBlue, HIGH); 
   delay(2000);
}
 motorCar.write(servoPosicion); 
 Serial.print(servoPosicion);
 Serial.print("degrees, with EMG: "); 
 Serial.println(EMGsig); 
  // Mostramos servo y sensor EMG
 delay(1000); 
}


void Mover_Adelante()
{
  digitalWrite (Pin_Motor_Der_A, HIGH);
  digitalWrite (Pin_Motor_Der_B, LOW);
  digitalWrite (Pin_Motor_Izq_A, HIGH);
  digitalWrite (Pin_Motor_Izq_B, LOW);
}