#include <Servo.h>    
Servo motorCar;     
int EMGsig;           // variable para almacenar el valor del sensor
int servoPosicion;    // Posicion para angulo del motor
int rango = 100;  //Establecemos rango para comparar los movimientos del motor
int ledBlue = 13;      //conectamos led a este pin para visualizar cambios
void setup() {
 Serial.begin(9600); // Starting the communication with the computer
 motorCar.attach(9); // Tell the servo it is plugged into pin 9
}
void loop() {
 EMGsig = analogRead(A0); // Leemos valores del sensor EMG
if (EMGsig < rango){    
    servoPosition = 20;     
  } else{            
   servoPosition=map(EMGsig,rango,1023,20,160); 
   digitalWrite(ledBlue, HIGH); 
   delay(2000);
}
 motorCar.write(servoPosition); 
  // Move the servo to the ‘servoPosition’ degree
 Serial.print(servoPosition);Serial.print(“ degrees, with EMG: “);Serial.println(EMGsig); 
  // Mostramos servo y sensor EMG
 delay(1000); 
}